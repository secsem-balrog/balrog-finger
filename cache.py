from google.appengine.api import memcache
import logging
import time
import utils

#####################################################################
#########          Caching                                 ##########
#####################################################################
# not used
#todo make time optional
def read_cache(key, get_new_val=None):
    cval = memcache.get(key)
    logging.error('\nget val: %s' % repr(cval))
    if not cval or not cval[1]:
        assert get_new_val, 'get_new_val function needed'
        cval = set_cache(key, get_new_val())
        logging.error('\nset new val: %s' % repr(cval))
    return cval

def set_cache(key, new_val, cache_time=True):
    logging.error('\nset_cache ' + key)
    cval = new_val
    if cache_time:
        cval = time.time(), cval
    memcache.set(key, cval)
    return cval
#todo write update cache function

def flush_cache():
    while not memcache.flush_all():
        pass

#caching Tor nodes
thor_addr = "https://check.torproject.org/exit-addresses"
thor_key = "THOR_KEY"

#race conditions are ignored
#TODO: race conditions
def tor_read_cache():
    val = memcache.get(thor_key)
    cur_time = time.time()
    # 1800 - hour / 2
    if val is not None and cur_time - val[0] < 1800 and val[1] != "":
        return val[1]
    val = utils.read_url(thor_addr)
    if val is None:
        val = ""
    val = cur_time, val
    if not memcache.set(thor_key, val):
        logging.error("Memcache set failed")
    return val[1]
