# This resource is for detecting the same operations systems and the same browsers of users.
# for each of values of this dictionary there is an array of different arrays of criterias.
# array of criteria is treated as successful if every criteria is satisfied, and it will lead to positive answer on
# question "are chosen sessions is from the same os/browser"
# As you maybe already understood - the answer to question will be successful if there exist any array of criterias that
# is all satisfied, ANY array of ALL criterias
fingerprint_comparison_measures = [{
    "same_os":[
        ["cookie", "Platform"],
        ["ip", "os_user_agent"]
    ],
    "same_browser":[
        ["cookie", "browser_user_agent"],
        ["ip", "browser_user_agent"]
    ]
}]