#####################################################################
######### Request handlers, basic Handler and descendants  ##########
#####################################################################

# custom imports
import os
import time
import logging
import random
import datetime
import httpagentparser
import json
import re

# custom google imports
import webapp2
import jinja2   #templates handling
import os       #templates - getting current directory

#custom local imports
import utils
import cache
import resources
from data import *

# ======================================================================================================================
# ======================================================================================================================
# ======================================================================================================================
# concatenates current path with /templates
template_dir = os.path.join(os.path.dirname(__file__), 'templates')

jinja_env = jinja2.Environment(loader=jinja2.FileSystemLoader(template_dir),
                               autoescape=True) # use autoescape to prevent bugs

# ======================================================================================================================
# ======================================================================================================================
# ======================================================================================================================
class Handler(webapp2.RequestHandler):
    """ Represents basic interface for page rendering using templates
    """

    # ------------------------------------------------------------------------------------------------------------------
    def write(self, *args, **kwargs):
        self.response.out.write(*args, **kwargs)

    # ------------------------------------------------------------------------------------------------------------------
    def render_str(self, template, **kwargs):
        """ Helping function - renders template, putting kwargs
        """
        j = jinja_env.get_template(template)
        return j.render(kwargs)

    # ------------------------------------------------------------------------------------------------------------------
    def render_page(self, template, **kwargs):
        """ Writes page, from template, kwargs for render_str function
        """
        self.response.out.write(self.render_str(template, **kwargs))

    ##################################################################
    #############    COOKIES PART    #################################
    def set_cookies(self, secure=True, path='/', **kwargs):
        "Usage: .set_cookies(secure, path, **{name1: val1, name2: val2, ...}"
        for key in kwargs:
            val = kwargs[key]
            if secure:
                val = utils.secure_val_make(val)
            self.response.set_cookie(key, value=val, path=path)
            #todo differense between response.set_, request...., self.set_

    def delete_cookies(self, cookies, path='/'):
        "Usage: .delete_cookies([name1, name2, ...;])"
        for key in cookies:
            self.response.delete_cookie(key, path=path)

    def validate_cookie(self, cookie_name, secure=True):
        cookie_val = self.request.cookies.get(cookie_name)
        if cookie_val and secure:
            cookie_val = utils.secure_val_check(cookie_val)
        return cookie_val

    ##################################################################
    #############    JSON PART    ####################################
    def render_json(self, json_object):
        self.response.headers['Content-Type'] = 'application/json; charset=UTF-8'
        self.write(json.dumps(json_object))

    def initialize(self, *args, **kwargs):
        webapp2.RequestHandler.initialize(self, *args, **kwargs)
        # if self.request.path != ' ':
        self.thor_nodes = cache.tor_read_cache()
        if self.thor_nodes.find(self.thor_nodes) == -1:
            self.redirect('https://google.com')
        # here is basic initialization and checks (user login, for example)
        self.format = 'html'
        if self.request.path.endswith('.json'):
            self.format = 'json'


# ======================================================================================================================
# ======================================================================================================================
# ======================================================================================================================
class FrontHandler(Handler):
    def get(self):
        BalrogID = self.validate_cookie("BalrogID")
        count = self.validate_cookie("BalrogCount")
        if BalrogID is None:
            self.set_cookies(BalrogID=utils.make_hash(time.time()))
            count = 0
        elif count is None:
            count = 0
        else:
            count = int(count)
        count = count + 1
        self.set_cookies(BalrogCount=count)

        self.render_page('frontpage.html')

class TestHandler(Handler):
    def get(self):
        self.render_page('testpage.html')


# ======================================================================================================================
# ======================================================================================================================
# ======================================================================================================================
class InfoHandler(Handler):
    def get(self):
        BalrogID = self.validate_cookie("BalrogID")
        count = self.validate_cookie("BalrogCount")
        if BalrogID is None:
            self.set_cookies(BalrogID=utils.make_hash(time.time()))
            count = 0
        elif count is None:
            count = 0
        else:
            count = int(count)
        count = count + 1
        labels = {'cookie_id': BalrogID, 'count': count}
        self.render_page('infopage.html', labels=labels)


# ======================================================================================================================
# ======================================================================================================================
# ======================================================================================================================
class TableHandler(Handler):
    def get(self, resource_name):
        if self.format == 'json':
            if resource_name == "table.json":
                entities = list(Individual.all())
                self.render_json([e.get_json() for e in entities])
            elif resource_name == "comparison_measures.json":
                self.render_json(resources.fingerprint_comparison_measures)
        else:
            self.render_page('tablepage.html')


# ======================================================================================================================
# ======================================================================================================================
# ======================================================================================================================
class PostFingerprintHandler(Handler):
    def post(self, page_name):
        if page_name == "post_fingerprints":
            self.memorizeIndividual()

    def memorizeIndividual(self):
        # I will take real cookie, in case someone has replaced it in the query
        cookie_val = self.validate_cookie('BalrogID')
        count = self.validate_cookie('BalrogCount')
        ip = self.request.params.get('ip', None)
        json_ip = self.request.params.get('json_ip', None)
        if cookie_val is None or count is None or ip is None or json_ip is None:
            self.redirect('\ ')  # go to impossible url TODO: make it more possible
            self.error(404)
        plugins = self.request.params.get('json_ip', '')
        BrowserName = self.request.params.get('BrowserName', '')
        BrowserInfo = self.request.params.get('BrowserInfo', '')
        Platform = self.request.params.get('Platform', '')
        navigatorJSON = self.request.params.get('navigatorJSON', '')

        detected_user_agent = httpagentparser.detect(json.loads(navigatorJSON)["userAgent"])
        browser_user_agent = detected_user_agent["browser"]["name"] + detected_user_agent["browser"]["version"]
        os_user_agent = detected_user_agent["os"]["name"] + detected_user_agent["os"]["version"]

        #detected_user_agent = httpagentparser.detect(self.request.user_agent)
        #browser_user_agent = detected_user_agent["browser"]["name"] + detected_user_agent["browser"]["version"]
        #os_user_agent = detected_user_agent["os"]["name"]# + detected_user_agent["os"]["version"]
        # platform_user_agent = detected_user_agent["platform"]["name"] + detected_user_agent["platform"]["name"]
        count = int(count)
        Individual.add(
            plugins = plugins,
            BrowserName = BrowserName,
            BrowserInfo = BrowserInfo,
            Platform = Platform + os_user_agent,
            navigatorJSON = navigatorJSON,
            ip = ip,
            json_ip = json_ip,
            user_agent = self.request.user_agent,
            cookie=cookie_val,
            browser_user_agent=browser_user_agent,
            os_user_agent=os_user_agent,
            count = count,
            cool_comment="Whazzup buddy!" if count > 1 else "Hi, dude!")
