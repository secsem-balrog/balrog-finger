var app = angular.module("infopage", []);
var ip_obj = JSON.parse(requestJsonIp());

app.controller("BrowserFingerPrintCtrl", [function() {
    this.fingerprints = {
        "cookie" : document.cookie,
        "userAgent" : window.navigator.userAgent,
        "ip" : ip_obj.ip,
        "country" : ip_obj.country_name,
        "city" : ip_obj.city,
        "plugins" : getPlugins(),
        "BrowserName" : getBrowserName(),
        "BrowserInfo" : getBrowserInfo(),
        "Platform" : getPlatform(),
        "navigatorJSON": JSON.stringify(navigatorObj())
    };
}]);
