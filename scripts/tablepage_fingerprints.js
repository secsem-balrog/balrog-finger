// ============================================ APPLICATION CREATION ===================================================
var app = angular.module("tablepage", ["ngTable"]);

// ============================================ PROVIDERS CREATION =====================================================
app.provider("calculateComparisonResults", function(){
    var comparison_measures = {};

    this.$get = ["$http", function($http){
        $http.get("/comparison_measures.json").success(function(json_data){
            comparison_measures = json_data[0];
        });

        return {
            recompute: function(selected_sessions){
                comparison_results = {
                    same_os:"None",
                    same_browser:"None"
                };

                for (var key in comparison_results)          // key e.g. "same_os"
                {
                    for (var measures in comparison_measures[key])  // comparison_measures[key] e.g. [["cookie", "os_user_agent"], ...]
                    {
                        comparison_results[key] = true;
                        comparison_measures[key][measures].forEach(function (criteria, index, arr){  // criteria e.g. "cookie"
                            for (var i1 = 0; i1 < selected_sessions.length; i1++)
                                for (var i2 = i1 + 1; i2 < selected_sessions.length; i2++)
                                    if (selected_sessions[i1][criteria] != selected_sessions[i2][criteria])
                                        comparison_results[key] = false;
                        });
                        // If for any of measure in measures will succeed then we will stop checking that type of measures
                        if (comparison_results[key] == true)
                            break;
                    }
                }
                return comparison_results;
            }
        };
    }];
});

// ============================================ CONTROLLERS CREATION ===================================================
app.controller("TablePageCtrl", ["$scope", "$http", "$filter", "ngTableParams", "calculateComparisonResults",
                                function($scope, $http, $filter, ngTableParams, calculateComparisonResults) {
    // I used old-style Contoller syntax, because unfortunately ng-table does not work with new-style (at least sorting does not work)

    // Looks like it is critical for ng-table to name data as "data"
    var data = [];
    // This is a terrible crutch - and it does not solve the problem completely
    // But it is problem relative to bad implementation of ng-table - because it will not create pagination, because I
    // load my data asynchronousily, and it happens, that I have no idea of its length - but I have to point it out in the
    // ngTableParams while creating.
    data.length = 10000;

    var selected_sessions = [];
    $scope.comparison_results = calculateComparisonResults.recompute(selected_sessions);

    $scope.changeSelection = function(session) {
        if (session.$selected != true) // Be careful with undefined
            selected_sessions.push(session);
        else
            selected_sessions.splice(selected_sessions.indexOf(session), 1);
        session.$selected = !session.$selected;

        $scope.comparison_results = calculateComparisonResults.recompute(selected_sessions);
    };

    $http.get("/table.json").success(function(json_data){
        data = json_data;

        // We must reload table, or it will be empty
        $scope.tableParams.total = data.length;
        $scope.tableParams.reload();
    });

    $scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10,          // count per page
        sorting: {
            time_mark: 'desc'   // initial sorting
        }
    }, {
        total: data.length,
        getData: function($defer, params) {
            var orderedData = params.sorting() ?
                    $filter('orderBy')(data, params.orderBy()) :
                    data;
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });
}]);

// ============================================  =======================================================================