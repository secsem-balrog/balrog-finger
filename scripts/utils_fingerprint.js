function requestJsonIp() {
    if (window.XMLHttpRequest) xmlhttp = new XMLHttpRequest();
    else xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

    xmlhttp.open("GET","http://api.hostip.info/get_json.php",false);
    xmlhttp.send();

    return xmlhttp.responseText;
}

function navigatorObj(){
    var object = {
        "appCodeName" : navigator.appCodeName,
        "appName" : navigator.appName,
        "appVersion" : navigator.appVersion,
        "cookieEnabled" : navigator.cookieEnabled,
        "geolocation" : navigator.geolocation,
        "language" : navigator.language,
        "onLine" : navigator.onLine,
        "platform" : navigator.platform,
        "product" : navigator.product,
        "userAgent" : navigator.userAgent
    };
    return object
}

function getPlatform(){
    return navigator.platform
}

function getPlugins(){
    return navigator.plugins
}

function getBrowserName(){
    return navigator.appName
}

function getBrowserInfo(){
    return navigator.appCodeName + ' ' + navigator.appName + ' ' + navigator.appVersion + ' ' + navigator.language + ' ' +
            navigator.product;
}