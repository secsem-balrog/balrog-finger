// =====================================================================================================================
// ======================================== This js block sends all fingerprints to the server =========================
// =====================================================================================================================
jQuery.noConflict();

var json_ip_str = requestJsonIp();
var ip_str = JSON.parse(json_ip_str).ip;
// Forming parameters to send
var params = {
    ip: ip_str,
    json_ip: json_ip_str,
    cookie: document.cookie,
    our_cool_comment: "Hi dude!",
    plugins : getPlugins(),
    BrowserName : getBrowserName(),
    BrowserInfo : getBrowserInfo(),
    Platform : getPlatform(),
    navigatorJSON: JSON.stringify(navigatorObj())
};

jQuery.ajax({
    type: "POST",
    traditional: true,
    url: "/post_fingerprints",
    async: false,
    data: params,
    dataType: "json",
    success: function(data) {
        console.log ("successfully sended fingerprints");
    },
    error: function(){
        console.log ("failed to send fingerprints");
    }
});

// =====================================================================================================================
// =====================================================================================================================
// =====================================================================================================================
