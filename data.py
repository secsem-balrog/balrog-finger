#####################################################################
######### Database classes (from db.Model)                 ##########
#####################################################################

# custom imports
import logging
import utils

# custom google imports
from google.appengine.ext import db

#custom local imports

# ======================================================================================================================
# ======================================================================================================================
# ======================================================================================================================
def blog_key(name='default'):
    return db.Key.from_path('blogs', name)

# ======================================================================================================================
# ======================================================================================================================
# ======================================================================================================================
class BasicModel(db.Model):
    # ------------------------------------------------------------------------------------------------------------------
    @classmethod
    def get_by_properties(cls, log=True, order=None, **kwargs):
        query = cls.filter_by_properties(log, **kwargs)
        if order:
            query = query.order(order)
        return query.get()

    # ------------------------------------------------------------------------------------------------------------------
    @classmethod
    def filter_by_properties(cls, log=True, **kwargs):
        query = cls.all()
        for key in kwargs:
            if key not in cls._all_properties and log:
                logging.error('%s.filter_by_properties: search for impossible key' % cls.__name__)
                continue
            query = query.filter(key + ' =', kwargs[key])
        return query

    # ------------------------------------------------------------------------------------------------------------------
    @classmethod
    def add(cls, **kwargs):
        logging.error('%s add' % cls.__name__)
        entity = cls(**kwargs)
        entity.put()
        return entity


# ======================================================================================================================
# ======================================================================================================================
# ======================================================================================================================
# This is class which instances will be stored in db
class Individual(BasicModel):
    #date time is better to do automatically
    # time_mark = db.DateTimeProperty(required=True)
    time_mark = db.DateTimeProperty(auto_now_add=True)  # will be filled automatically
    # new field
    last_modified = db.DateTimeProperty(auto_now=True)  # will be filled automatically
    # ip address - second required field in table
    ip = db.StringProperty()
    json_ip = db.StringProperty()
    user_agent = db.StringProperty()

    cookie = db.StringProperty(required=True)
    browser_user_agent = db.StringProperty(required=True)
    os_user_agent = db.StringProperty(required=True)
    count = db.IntegerProperty()
    cool_comment = db.StringProperty(required=True)

    BrowserName = db.StringProperty()
    BrowserInfo = db.StringProperty()
    Platform = db.StringProperty()
    navigatorJson = db.StringProperty()
    plugins = db.TextProperty()

    @classmethod
    def make_valid(cls, val):
        return val[:400] # 500 is max for strings

    @classmethod
    def add(cls, **kwargs): #TODO: owerride using basic method
        logging.error('%s add' % cls.__name__)
        for k in kwargs:
            if isinstance(kwargs[k], basestring) and k != 'plugins':
                kwargs[k] = cls.make_valid(kwargs[k])
        entity = cls(**kwargs)
        entity.put()
        return entity

    # ------------------------------------------------------------------------------------------------------------------
    def get_json(self):
        res = {}

        res['BrowserName'] = str(self.BrowserName)
        res['BrowserInfo'] = str(self.BrowserInfo)
        res['Platform'] = str(self.Platform)
        res['navigatorJson'] = str(self.navigatorJson)
        res['plugins'] = str(self.plugins)

        res['time_mark'] = str(self.time_mark)
        res['last_modified'] = str(self.last_modified)
        res['ip'] = str(self.ip)
        res['user_agent'] = str(self.user_agent)
        res['cookie'] = str(self.cookie)
        res['browser_user_agent'] = str(self.browser_user_agent)
        res['os_user_agent'] = str(self.os_user_agent)
        res['count'] = str(self.count)
        res['cool_comment'] = str(self.cool_comment)
        return res