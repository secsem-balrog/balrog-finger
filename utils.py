import re
import hmac
import random
import string
import hashlib
import urllib2
import logging

# different utils, given for example from previous project

#####################################################################
######### String matching - username, password, email      ##########
#####################################################################

NAME_RE = re.compile(r"^[a-zA-Z0-9_-]{3,20}$")
def valid_username(str):
    return str and NAME_RE.match(str)

PASS_RE = re.compile(r"^.{3,20}$")
def valid_password(str):
    return str and PASS_RE.match(str)

EMAIL_RE = re.compile(r"^[\S]+@[\S]+\.[\S]+$")
def valid_email(str):
    return str == '' or EMAIL_RE.match(str)

# page url re:
PAGE_RE = r'(/(?:[a-zA-Z0-9_-]+/?)*)'
#r'(/[a-zA-Z0-9]*)(?:/?)'

#####################################################################
######### Secure module - make secure vals with secret     ##########
#####################################################################

skey= 'Unicorn'

def secure_val_make(val, secret=skey):
    return '%s|%s' % (val, hmac.new(skey, str(val)).hexdigest())

def secure_val_check(sec_val, secret=skey):
    try: # sec_val can be string, but not type str in this gae
        sec_val = str(sec_val)
        val = sec_val[:sec_val.find('|')]
        if sec_val == secure_val_make(val, skey):
            return val
    except:
        return None

def make_hash(val):
    return hmac.new(skey, str(val)).hexdigest()

#####################################################################
######### Secure module - hashing with salt                ##########
#####################################################################

def make_salt(length=5):
    return ''.join(random.choice(string.letters) for i in xrange(length))

#todo make hash without name
def make_pw_hash(name, pw, salt = make_salt()):
    h = hashlib.sha256(name + pw + salt).hexdigest()
    return salt + ',' + h

def valid_pw_hash(name, pw, pw_hash):
    return pw_hash == make_pw_hash(name, pw, pw_hash[:pw_hash.rfind(',')])

#####################################################################
######### Url handling with urllib2                        ##########
#####################################################################

def read_url(url):
    """ Opens url, which can be a string or a Request object
    """
    try: response = urllib2.urlopen(url)
    except urllib2.URLError as e:
        logging.error(e.errno)
    if response.getcode() < 300:
        return response.read()